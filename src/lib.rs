#![crate_type = "staticlib"]

extern crate rlibc;
use std::ffi::CString;

#[no_mangle]
pub extern fn double_input(input: i32) -> i32 {
    input * 2
}

#[no_mangle]
pub extern fn get_message_from_rust() -> *const u8 {
    let foo = b"I am a string literal from Rust\0";
    foo.as_ptr()
}

#[no_mangle]
pub extern fn get_message_from_rust_cstring() -> *const u8 {
    let foo = CString::new("I am a CString from Rust").unwrap();
    foo.as_ptr()
}
