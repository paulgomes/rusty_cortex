all: \
target/release/rusty_cortex.o \
target/release/rusty-cortex-arm-unknown-linux-gnueabi.ll \
target/arm-unknown-linux-gnueabi/release/librusty_cortex.a
	arm-none-eabi-objdump -S target/release/rusty_cortex.o > target/release/rusty_cortex.objdump

target/release/rusty-cortex-arm-unknown-linux-gnueabi.ll: src/lib.rs target/arm-unknown-linux-gnueabi/release/librusty_cortex.a
	rustc --emit llvm-ir --target=arm-unknown-linux-gnueabi \
	-C panic=abort \
	-L $(HOME)/.xargo/lib/rustlib/arm-unknown-linux-gnueabi/lib \
	-L crate=target/arm-unknown-linux-gnueabi/release/deps \
	--extern panic_unwind=$(HOME)/.xargo/lib/rustlib/arm-unknown-linux-gnueabi/lib/libstd.rlib \
	-o target/release/rusty-cortex-arm-unknown-linux-gnueabi.ll \
	src/lib.rs

target/release/rusty_cortex.s: target/release/rusty-cortex-arm-unknown-linux-gnueabi.ll
	llc-4.0 -march=thumb -mcpu=cortex-m3 -asm-verbose \
	target/release/rusty-cortex-arm-unknown-linux-gnueabi.ll \
	-o target/release/rusty_cortex.s

target/release/rusty_cortex.o: target/release/rusty_cortex.s
	arm-none-eabi-gcc -mthumb -mcpu=cortex-m3 -O0 -Wall -c -nodefaultlibs -nostartfiles -o target/release/rusty_cortex.o target/release/rusty_cortex.s
	cp -rf target/release/rusty_cortex.o $(HOME)/bitbucket/contiki-n-rust/examples/cc26xx/rusty_cortex.o

target/arm-unknown-linux-gnueabi/release/librusty_cortex.a: src/lib.rs Cargo.toml
	xargo build --target arm-unknown-linux-gnueabi --release --verbose
	cp -rf target/arm-unknown-linux-gnueabi/release/librusty_cortex.a $(HOME)/bitbucket/contiki-n-rust/examples/cc26xx/librusty_cortex.a

dependencies:
	rustup component add rust-src

clean:
	rm -rf target
